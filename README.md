
# An Angular 4 Application with ASP.NET Core 2.1

## Division Controller

* We are using ASP.NET Core 2.1 as backend and this C# code is used to implement CRUD operation on data provided from frontend.

## Startup file

* It is the entry point of the application. It configures services & the request pipeline which handles all requests made to the application. 

## Division-component

* The folder has angular code, division module has a list page to show all the divisions coming from db implemented in controller and form page has functionality to add or update a division.
* Service is used to implement CRUD functionality.

Folder has following files:

1. division.component.html
2. division.component.ts
3. division-form.component.html
4. division-form.component.ts
5. divisions.module.ts
6. divisions.routing.ts
7. divisions.service.ts