import { Component, OnInit, Pipe } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import '@app/helpers/rxjs-operators';

import { Division, ViewMode } from '@app/models';
import { DivisionsService } from '@app/services';

@Component
({
    templateUrl: './division.component.html'
})

export class DivisionsComponent implements OnInit {

    public ViewMode: any = ViewMode;

    viewMode: any = ViewMode.PRELOADING;
    divisions: Division[];
    selectedDivision: Division;
    search: string;
    

    constructor(private dataService: DivisionsService) {

    }

    ngOnInit() {
        this.initialize();
    }

    private initialize() {
        this.viewMode = ViewMode.PRELOADING;

        this.dataService
            .getDivisions()
			.subscribe(data => {
                this.divisions = data;
            },
            error => { console.log("Error: ", error); },
            () => { this.showList(); }
            );

        this.selectedDivision = undefined;
    }

    showList() {
        this.selectedDivision = undefined;
        this.viewMode = ViewMode.LIST;
    }

    select(division: Division) {
        this.viewMode = ViewMode.EDIT;
        this.selectedDivision = division;
    }

    delete(division: Division) {

        this.dataService.deleteDivision(division).subscribe(
            data => { },
            error => { console.log("Error: ", error); },
            () => {
                let index: number = this.divisions.indexOf(division);
                if (index !== 1) {
                    this.divisions.splice(index, 1);
                }
            }
        );
    }

    addDivision() {
        this.viewMode = ViewMode.ADD;
        this.selectedDivision = undefined;
    }

    newDivision() {
        this.initialize();
    }

    editDivision(division: Division) {
        let p = this.divisions.find(x => x.id == division.id);
        p.name = division.name;
        this.showList();
    }
}
