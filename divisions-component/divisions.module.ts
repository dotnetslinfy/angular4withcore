import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Router
import { DivisionsRoutingModule } from './divisions.routing';

// Module Components
import { DivisionsComponent } from './division.component';
import { DivisionFormComponent } from './division-form.component';
import { DivisionProductComponent } from './division-product.component';

// Common Components
import { ExcelModule } from '@app/controls/excel.module';

// Services
import { DivisionsService, ProductsService } from '@app/services';

//import { OrderByPipe } from 'app/common/pipes/orderby.pipe';
import { PipesModule } from '@app/pipes';


@NgModule
({
	imports:
	[
        CommonModule,
        FormsModule,
		ReactiveFormsModule,
		DivisionsRoutingModule,
		PipesModule,
		ExcelModule
	],
	declarations:
	[
		DivisionsComponent,
		DivisionFormComponent,
		DivisionProductComponent
	],
	providers:
	[
		DivisionsService,
		ProductsService
	]
})

export class DivisionsModule { }
