import { Component, Input, OnChanges, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Division, Product, User } from '@app/models';
import { DivisionsService, ProductsService } from '@app/services';

@Component
({
    templateUrl: './division-form.component.html',
    selector: 'divisions-form'
})

export class DivisionFormComponent implements OnChanges {
    @Input() division: Division;
    @Input() formType: string;
    @Output() cancelDivision: EventEmitter<void> = new EventEmitter<void>();
    @Output() newDivision: EventEmitter<Division> = new EventEmitter<Division>();
    @Output() editDivision: EventEmitter<Division> = new EventEmitter<Division>();

    divisionForm: FormGroup;

    formSubmitType: string;
    isAdd: boolean = true;
	products: Product[];
	isError: boolean = false;
	ErrorMessage = "";

    selectedProducts: Product[];

    constructor(private fb: FormBuilder, private dataService: DivisionsService, private productService: ProductsService) {
        this.products = [];
        this.selectedProducts = [];

        this.createForm();
    }

    createForm() {
        this.productService.getProducts().subscribe(
            data => { this.products = data; },
            error => { console.log("Error"); },
            () => { }
        );

        this.divisionForm = this.fb.group({
            id: [null],
            name: [null, Validators.required],
            products: this.fb.array([]),
        });
    }

    ngOnChanges() {
        this.resetForm();
        this.isAdd = (this.formType.toUpperCase() === 'ADD');
    }

    private resetForm() {
        if (this.division) {
            this.dataService.getProducts(this.division.id).subscribe(
                data => { this.selectedProducts = data },
                error => { console.log("Error"); },
                () => {
                    const control = <FormArray>this.divisionForm.controls['products'];
                    for (let i in this.selectedProducts) {
                        const addCtrl = this.initProducts(this.selectedProducts[i]);
                        control.push(addCtrl);
                    }
                }
            );

            this.divisionForm.reset({
                id: this.division.id,
                name: this.division.name
            });
        }
    }

    onSubmit() {
        this.division = this.divisionForm.value;

        if (this.formSubmitType == "POST") {
            this.dataService.addDivision(this.division).subscribe(
                data => { },
				error => {
					console.log("error");
					this.isError = true;
					this.ErrorMessage = error.error.message;},
                () => { this.newDivision.emit(this.division); }
            );

        }
        else if (this.formSubmitType == "PUT") {
            this.dataService.updateDivision(this.division).subscribe(
                data => { },
				error => {
					console.log("Error");
					this.isError = true;
					this.ErrorMessage = error.error.message;
				},
				() => { debugger; this.newDivision.emit(this.division); }
            );
		}
		else if (this.formSubmitType == 'DELETE') {
			if (confirm('Are you sure you want to delete this record?')) {

				this.dataService.deleteDivision(this.division).subscribe(
					data => { },
					error => {
						console.log('error');
						this.isError = true;
						this.ErrorMessage = error.error.message;
					},
					() => { this.newDivision.emit(); }
				);
			}
		}
    }

    submitFormType(method: string) {
        this.formSubmitType = method;
        this.onSubmit();
    }

    cancel() {
        this.divisionForm.reset();
        this.division = undefined;
        this.cancelDivision.emit();
    }

    initProducts(product: Product) {
        return this.fb.group({
            productid: [product.id],
            name: [product.name]
        });
    }

    addProduct(product: string) {
        
        let findProduct = this.selectedProducts.find(x => x.id == product);
        if (findProduct != null) {
            console.log("Product already added in the list");
            return;
        }

        let m = this.products.find(x => x.id === product);
        this.selectedProducts.push(m);
        const control = <FormArray>this.divisionForm.controls['products'];
        const addCtrl = this.initProducts(m);

        control.push(addCtrl);
    }

    removeProduct(i: number) {
        let m = this.products.find(x => x.id === i.toString());
        this.selectedProducts.splice(i, 1);

        const control = <FormArray>this.divisionForm.controls['products'];
        control.removeAt(i);
    }
}
