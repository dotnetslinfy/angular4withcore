import { environment } from '@environment';

import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { BaseService } from '@app/services';

import { User, Division, Product } from '@app/models';

@Injectable()
export class DivisionsService extends BaseService
{
	constructor
	(
		http: HttpClient
	)
	{
		super
		(
			http,
			environment.api.baseUrl + '/divisions'
		);
	}

	public getDivisions(): Observable<Division[]>
	{
		return this.findEntities<Division>();
	}

	public addDivision(division: Division): Observable<Division>
	{
		return this.addEntity<Division>(division);
	}

	public updateDivision(division: Division): Observable<Division>
	{
		return this.updateEntity<Division>
		(
			division,
			division.id.toString()
		);
	}

	public deleteDivision(division: Division): Observable<Division>
	{
		return this.deleteEntity<Division>
		(
			division,
			division.id.toString()
		);
	}

	public getProducts(divisionId: string): Observable<Product[]>
	{
		let endpoint = environment.api.baseUrl + '/divisions/products/' + divisionId;
		//let endpoint = this.endpoint + '/' + divisionId + '/products';

		return this.findEntities<Product>
		(
			new HttpParams(),
			endpoint
		);
	}

	//public getProducts(division: Division): Observable<Product[]>
	//{
	//	this.setHeaders();

	//	let options = new RequestOptions({ headers: this.headers });
	//	let endpoint = this.endpoint + '/' + division.id + '/products';

	//	return this.http
	//		.get(endpoint, options)
	//		.map(res => (res.json() as Product[]))
	//		.catch(this.handleError);
	//}
}
