﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//import { AuthGuard } from './services/auth.guard';

import { DivisionsComponent } from './division.component';

// Route Configuration
const routes: Routes =
[
	{ path: '', component: DivisionsComponent }
];

@NgModule({
	imports: [
		RouterModule.forChild(routes)
	],
	exports: [RouterModule]
})
export class DivisionsRoutingModule { }