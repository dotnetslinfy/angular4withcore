﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

using Servier.Reach.Api.Helpers;
using Servier.Reach.Api.Filters;

using Servier.Reach.Core.Models;
using Servier.Reach.Core.Repository;

namespace Servier.Reach.Api.Controllers
{
	//[Authorize]
	[AllowAnonymous]
	[Controller]
	[Route("[controller]/[action]")]
	public class DivisionsController : ControllerBase
	{
		private readonly IDivisionRepository _divisionRepository;

		public DivisionsController(IDivisionRepository divisionRepository)
		{
			_divisionRepository = divisionRepository;

		}

		[HttpGet]
		[NoCache]
		[Produces("application/json")]
		public IActionResult List()
		{
			try
			{
				var list = _divisionRepository.GetAll();
				return new JsonResult(list);
			}
			catch (Exception e)
			{
				return BadRequest(new
				{
					status = HttpStatusCode.BadRequest.ToString(),
					message = e.Message
				});
			}
		}

		[HttpGet("{id}")]
		[NoCache]
		[Produces("application/json")]
		public IActionResult Get(string id)
		{
			try
			{
				var list = _divisionRepository.Get(id);
				return new JsonResult(list);
			}
			catch (Exception e)
			{
				return BadRequest(new
				{
					status = HttpStatusCode.BadRequest.ToString(),
					message = e.Message
				});
			}
		}

		[HttpPost]
		[NoCache]
		[Produces("application/json")]
		public IActionResult Add([FromBody]FieldForceDivision division)
		{
			if (ValidationHelper.HasEmptyValues(new List<string>() { division.Name }))
			{
				return BadRequest(new
				{
					status = HttpStatusCode.BadRequest.ToString(),
					message = "Values cannot be empty"
				});
			}

			if (ModelState.IsValid)
			{
				try
				{
					var d = new FieldForceDivision()
					{
						Id = string.Empty,
						Name = division.Name
					};

					_divisionRepository.Create(d);
					string divId = d.Name.Substring(0, 1).ToUpper();

					if (division.Products.Count > 0)
					{
						var dpList = new List<FieldForceDivision_Product>();
						foreach (var p in division.Products)
						{
							var dm = new FieldForceDivision_Product()
							{
								DivisionId = divId,
								ProductId = p.ProductId
							};
							dpList.Add(dm);
						}
						_divisionRepository.AddProducts(dpList);
					}

					return Created($"/divisions/add/{divId}", d);
				}
				catch (Exception e)
				{
					return BadRequest(new
					{
						status = HttpStatusCode.BadRequest.ToString(),
						message = e.Message
					});
				}
			}
			return BadRequest(new
			{
				status = HttpStatusCode.BadRequest.ToString(),
				message = "Invalid ModelState"
			});
		}

		[HttpPut("{id}")]
		[NoCache]
		[Produces("application/json")]
		public IActionResult Update(string id, [FromBody]FieldForceDivision division)
		{
			if (division == null || division.Id != id)
			{
				return BadRequest(new
				{
					status = HttpStatusCode.NotFound.ToString(),
					message = "Division not found"
				});
				//return NotFound();
			}

			if (ValidationHelper.HasEmptyValues(new List<string>() { division.Id, division.Name }))
			{
				return BadRequest(new
				{
					status = HttpStatusCode.BadRequest.ToString(),
					message = "Values cannot be empty"
				});
			}

			if (ModelState.IsValid)
			{
				try
				{
					var d = _divisionRepository.Get(division.Id);
					if (d == null)
						return NotFound();

					d.Name = division.Name;
					_divisionRepository.Update(d);

					if (!string.IsNullOrWhiteSpace(d.Id))
					{
						_divisionRepository.DeleteProducts(d.Id);
					}

					if (division.Products.Count > 0)
					{
						var dmList = new List<FieldForceDivision_Product>();
						foreach (var m in division.Products)
						{
							var dm = new FieldForceDivision_Product()
							{
								DivisionId = d.Id,
								ProductId = m.ProductId
							};
							dmList.Add(dm);
						}
						_divisionRepository.AddProducts(dmList);
					}

					return Ok();
				}
				catch (Exception e)
				{
					return BadRequest(new
					{
						status = HttpStatusCode.BadRequest.ToString(),
						message = e.Message
					});
				}
			}
			return BadRequest(new
			{
				status = HttpStatusCode.BadRequest.ToString(),
				message = "Invalid ModelState"
			});
		}

		[HttpDelete("{id}")]
		[NoCache]
		[Produces("application/json")]
		public IActionResult Delete(string id)
		{
			try
			{
				var item = _divisionRepository.Get(id);
				_divisionRepository.Delete(item);
				return Ok();
			}
			catch (Exception e)
			{
				return BadRequest(new
				{
					status = HttpStatusCode.BadRequest.ToString(),
					message = e.Message
				});
			}
		}

		[HttpGet("{id}")]
		[NoCache]
		[Produces("application/json")]
		public IActionResult Products(string id)
		{
			try
			{
				var list = _divisionRepository.GetProducts(id);
				return new JsonResult(list);
			}
			catch (Exception e)
			{
				return BadRequest(new
				{
					status = HttpStatusCode.BadRequest.ToString(),
					message = e.Message
				});
			}
		}
	}
}
