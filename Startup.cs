using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.AspNetCore.SpaServices.AngularCli;

using Microsoft.Extensions.Logging;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

using Servier.Reach.Core.Models;
using Servier.Reach.Core.Repository;
using Servier.Reach.Core.Services;

using Servier.Reach.Infrastructure.Messaging;
using Servier.Reach.Infrastructure.Repository;
using Servier.Reach.Infrastructure.Services;

using Servier.Reach.WebClient.FeatureFolders;
using Servier.Reach.WebClient.Account.Data;
using Servier.Reach.WebClient.Account.Models;
using Servier.Reach.WebClient.Account.Services;


namespace Servier.Reach.WebClient
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			var connectionString = Configuration.GetConnectionString("DefaultConnection");

			services.AddDbContext<AccountContext>(options =>
				options.UseSqlServer(connectionString));

			services.AddDbContext<ApplicationContext>(options =>
				options.UseSqlServer(connectionString));

			services.AddIdentity<ApplicationUser, ApplicationRole>
				(
					config => { config.SignIn.RequireConfirmedEmail = true; }
				)
				.AddEntityFrameworkStores<AccountContext>()
				.AddDefaultTokenProviders();

			services
				.AddMvc()
				.AddFeatureFolders()
				.AddJsonOptions(options =>
				{
					options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
					options.SerializerSettings.DefaultValueHandling = DefaultValueHandling.Include;
					options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
				});

			services
				.AddRouting(options =>
				{
					options.LowercaseUrls = true;
				});

			services.AddTransient<Servier.Reach.WebClient.Account.Services.IEmailSender, Servier.Reach.WebClient.Account.Services.EmailSender>();

			services.AddTransient<IExcelService, ExcelService>();
			services.AddTransient<IPdfService, PdfService>();
			services.AddTransient<IEmailNotification, EmailNotification>();

            services.AddTransient<IDivisionRepository, DivisionRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
		{
			loggerFactory.AddConsole(Configuration.GetSection("Logging"));
			loggerFactory.AddDebug();

			if (env.IsDevelopment())
			{
				app.UseBrowserLink();
				app.UseDeveloperExceptionPage();
				app.UseDatabaseErrorPage();
			}
			else
			{
				app.UseExceptionHandler("/Home/Error");
			}

			//app.UseDefaultFiles();
			app.UseStaticFiles();

			// For Angular
			var provider = new FileExtensionContentTypeProvider();
			provider.Mappings.Remove(".html");
			app.UseStaticFiles(new StaticFileOptions()
			{
				FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"Angular", @"dist")),
				ContentTypeProvider = provider
			});
			app.UseAuthentication();
			//app.UseMvcWithDefaultRoute();

			app.UseMvc(routes =>
			{
				routes.MapRoute(
				   name: "default",
				   template: "{controller=Main}/{action=Index}/{id?}");

				routes.MapSpaFallbackRoute(
					name: "spa-fallback",
					defaults: new { controller = "Main", action = "Index" });
			});

			app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "Angular";

                if (env.IsDevelopment())
                {
					//spa.UseAngularCliServer(npmScript: "start");
					//spa.UseAngularCliServer(npmScript: "watch");

					// Inconsistent. Needs investigation.
					// Use
					// >> ng buiid --watch
					// at console instead.
				}
            });
		}
	}
}
